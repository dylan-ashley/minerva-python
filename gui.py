from error import MoveError
import time


class GUI:

    def __init__(self):
        self.mySymbol = None

    def gameOver(self, board):
        self.printBoard(board)
        if board.getWinner() == self.mySymbol:
            print('Congratulations! You Won!')
        elif board.getWinner() == self.otherSymbol:
            print('You Lose!')
        else:
            print('Draw!')
        time.sleep(2)

    def gameStart(self, mySymbol, otherSymbol, startingPlayer):
        self.mySymbol = mySymbol
        self.otherSymbol = otherSymbol
        print('Welcome to tic-tac-toe!\n')
        if self.mySymbol == startingPlayer:
            print('You will make the first move.')
        else:
            print('Your opponent will make the first move.')

    def getMove(self, board, firstCall=True):
        if firstCall:
            self.printBoard(board)
        move = input('Make your move: ')
        try:
            return self.textToMove(move)
        except:
            self.invalidMove(board, move)
            return self.getMove(board, firstCall=False)

    def invalidMove(self, board, move):
        print('You have supplied an invalid move!')

    def moveToText(self, move):
        return '{}{}'.format(['3', '2', '1'].index(move[0]) + 1,
                             ['A', 'B', 'C'][move[1] - 1])

    def printBoard(self, board):
        board = {(i, j): '-'
                 if board.getSquare((i, j)) is None
                 else board.getSquare((i, j))
                 for i in range(1, 4)
                 for j in range(1, 4)}
        print('3 {}  {}  {}'.format(board[(1, 1)],
                                    board[(1, 2)],
                                    board[(1, 3)]))
        print('2 {}  {}  {}'.format(board[(2, 1)],
                                    board[(2, 2)],
                                    board[(2, 3)]))
        print('1 {}  {}  {}'.format(board[(3, 1)],
                                    board[(3, 2)],
                                    board[(3, 3)]))
        print('  A  B  C')

    def textToMove(self, text):
        text = text.upper()
        if not len(text) == 2:
            raise MoveError(
                'Text is not of the correct length to be a valid move!')
        if text[0] in ['1', '2', '3'] and text[1] in ['A', 'B', 'C']:
            return (['3', '2', '1'].index(text[0]) + 1,
                    ['A', 'B', 'C'].index(text[1]) + 1)
        elif text[1] in ['1', '2', '3'] and text[0] in ['A', 'B', 'C']:
            return (['3', '2', '1'].index(text[1]) + 1,
                    ['A', 'B', 'C'].index(text[0]) + 1)
        else:
            raise MoveError('Not a valid move!')
