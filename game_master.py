from error import MoveError
import board
import random


class GameMaster:

    def __init__(self, playerOne, playerTwo):
        self.board = board.Board()
        self.playerOne = playerOne
        self.playerTwo = playerTwo
        self.turn = random.choice(['X', 'O'])

    def playGame(self):
        self.playerOne.gameStart('X', 'O', self.turn)
        self.playerTwo.gameStart('O', 'X', self.turn)
        while not self.board.gameOver():
            move = (self.playerOne.getMove(self.board)
                    if self.turn == 'X'
                    else self.playerTwo.getMove(self.board))
            try:
                self.board.makeMove((move, self.turn))
            except MoveError:
                if self.turn == 'X':
                    self.playerOne.invalidMove(self.board, move)
                else:
                    self.playerTwo.invalidMove(self.board, move)
            else:
                self.turn = 'O' if self.turn == 'X' else 'X'
        self.playerOne.gameOver(self.board)
        self.playerTwo.gameOver(self.board)
