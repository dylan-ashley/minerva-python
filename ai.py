import log
import solver


class AI:

    def __init__(self):
        self.log = log.Log('ai_log.txt')

    def invalidMove(self, board, move):
        self.log.write('Invalid Move!')

    def gameOver(self, board):
        pass

    def gameStart(self, mySymbol, otherSymbol, startingPlayer):
        self.mySymbol = mySymbol
        self.otherSymbol = otherSymbol

    def getMove(self, board):
        move = solver.get_move(board.gameBoard,
                               self.mySymbol,
                               self.otherSymbol)
        self.log.write('AI attempting move {}'.format(move))
        return move
