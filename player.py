import gui


class Player:

    def __init__(self):
        self.gui = gui.GUI()

    def invalidMove(self, board, move):
        self.gui.invalidMove(board, move)

    def gameOver(self, board):
        self.gui.gameOver(board)

    def gameStart(self, mySymbol, otherSymbol, startingPlayer):
        self.gui.gameStart(mySymbol, otherSymbol, startingPlayer)

    def getMove(self, board):
        return self.gui.getMove(board)
