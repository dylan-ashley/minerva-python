
"""
This module provides a simple logging system.
"""

# The MIT License (MIT)
#
# Copyright (c) 2016 Dylan Robert Ashley
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import datetime
import functools
import os
import sys


class Log:

    """
    Object representing a log file.
    """

    def __init__(self, filename):
        if __debug__:
            self.filename = self._get_filename(filename)
            self.write("Log created as {}".format(filename))

    def _get_filename(self, filename):
        filename = "logs/{}".format(filename)
        self._make_directories("/".join(filename.split("/")[:-1]))
        return filename

    def _get_iso_date_time(self):
        return datetime.datetime.today().isoformat()

    def _make_directories(self, path):
        if not os.path.exists(path):
            os.makedirs(path)

    def write(self, message):
        if __debug__:
            with open(self.filename, "a") as outfile:
                text = "[{}] {}\n".format(self._get_iso_date_time(), message)
                outfile.write(text)


def autologging(function):
    @functools.wraps(function)
    def wrapped(self, *args, **kwargs):
        args_list = list()
        for arg in args:
            arg = "'{}'".format(arg) if type(arg) is str else "{}".format(arg)
            args_list.append(arg)
        call_string = "{}({})".format(wrapped.__name__, ", ".join(args_list))
        self.log.write(call_string)
        rv = function(self, *args, **kwargs)
        self.log.write("{} returned {}".format(call_string, rv))
        return rv
    return wrapped
