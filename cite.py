#!/usr/bin/env python3 -O

"""
This module provides an easy way to aggregate citations.

A citation is considered to be a single line comment of two or three values
seperated by semicolons. If only two values appear it is interpreted as a URL
followed by a date. If three values appear it is interpreted as a name
followed by a URL and then a date.

Execute this program in the folder containing the files to search.
"""

# The MIT License (MIT)
#
# Copyright (c) 2016 Dylan Robert Ashley
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import glob
import os


def find_lines_from_file(file):
    lines = list()
    try:
        with open(file, "r") as infile:
            data = [line.strip() for line in infile]
            for line in data:
                if len(line) == 0:
                    continue
                elif line[0] == "#":
                    line = line[1:]
                elif line[:2] == "//":
                    line = line[2:]
                elif (line[:4] == "<!--") and (line[-3:] == "-->"):
                    line = line[4:-3]
                else:
                    continue
                if ((1 <= line.count(";") <= 2) and
                   (len(line[line.rfind(";") + 1:].strip()) == 10)):
                        lines.append(line)
    except UnicodeDecodeError:
        pass
    return lines


def find_lines_from_path(path):
    lines = list()
    for item in glob.glob("{}/*".format(path)):
        if os.path.isdir(item):
            for line in find_lines_from_path(item):
                lines.append(line)
        elif os.path.isfile(item):
            for line in find_lines_from_file(item):
                lines.append(line)
    return lines


def main():
    lines = list()
    print("<table>")
    print("\t<tr>")
    for item in ["Name", "URL", "Date Retrieved"]:
        print("\t\t<td nowrap=\"nowrap\">{}</td>".format(item))
    print("\t</tr>")
    for line in sorted(list(set(find_lines_from_path(
                       "{}".format(os.getcwd()))))):
        line = line.strip().split(";")
        if len(line) == 2:
            line = ["N/A", line[0], line[1]]
        print("\t<tr>")
        for item in line:
            print("\t\t<td nowrap=\"nowrap\">{}</td>".format(item.strip()))
        print("\t</tr>")
    print("</table>")

if __name__ == "__main__":
    main()
