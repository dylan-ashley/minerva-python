#!/usr/bin/env python3 -O

import ai
import game_master
import player
import random


def main():
    players = [ai.AI(), player.Player()]
    random.shuffle(players)
    gameMaster = game_master.GameMaster(*players)
    try:
        gameMaster.playGame()
    except (KeyboardInterrupt, EOFError):
        print()

if __name__ == '__main__':
    main()
