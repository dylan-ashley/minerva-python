from error import MoveError
import log


class Board:

    def __init__(self, gameBoard=None, winningStates=None):
        self.log = log.Log("board_log.txt")
        if gameBoard is None:
            self.gameBoard = {(1, 1): None,
                              (1, 2): None,
                              (1, 3): None,
                              (2, 1): None,
                              (2, 2): None,
                              (2, 3): None,
                              (3, 1): None,
                              (3, 2): None,
                              (3, 3): None}
        else:
            self.gameBoard = gameBoard

        if winningStates is None:
            self.winningStates = [((1, 1), (1, 2), (1, 3)),
                                  ((2, 1), (2, 2), (2, 3)),
                                  ((3, 1), (3, 2), (3, 3)),
                                  ((1, 1), (2, 1), (3, 1)),
                                  ((1, 2), (2, 2), (3, 2)),
                                  ((1, 3), (2, 3), (3, 3)),
                                  ((1, 1), (2, 2), (3, 3)),
                                  ((1, 3), (2, 2), (3, 1))]
        else:
            self.winningStates = winningStates

    def boardFull(self):
        for value in self.gameBoard.values():
            if value is None:
                return False
        return True

    def gameOver(self):
        if self.getWinner() is None:
            return self.boardFull()
        else:
            return self.getWinner()

    def getSquare(self, position):
        return self.gameBoard[position]

    def getWinner(self):
        for state in self.winningStates:
            first = self.getSquare(state[0])
            if ((first is not None) and
               (first == self.getSquare(state[1])) and
               (first == self.getSquare(state[2]))):
                return first

    def makeMove(self, move):
        if self.getSquare(move[0]) is None:
            self.setSquare(move[0], move[1])
        else:
            raise MoveError('Space occupied by {}!'.format(
                            self.getSquare(move[0])))

    def setSquare(self, position, value):
        self.gameBoard[position] = value
