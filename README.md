# minerva-python

This is a simple program intended to demonstrate a solution to tic-tac-toe.

It is written in python and can be run by executing main.py.


## Reuse Statement

In the completion of this project the following additional resources were used:

<table>
    <tr>
        <td nowrap="nowrap">Name</td>
        <td nowrap="nowrap">URL</td>
        <td nowrap="nowrap">Date Retrieved</td>
    </tr>
    <tr>
        <td nowrap="nowrap">N/A</td>
        <td nowrap="nowrap">https://en.wikipedia.org/wiki/Negamax</td>
        <td nowrap="nowrap">2016-03-03</td>
    </tr>
    <tr>
        <td nowrap="nowrap">N/A</td>
        <td nowrap="nowrap">https://en.wikipedia.org/wiki/Principal_variation_search</td>
        <td nowrap="nowrap">2016-03-03</td>
    </tr>
</table>

Thank you to Martin Mueller (mmueller@ualberta.ca) for helping to find a bug in the alpha-beta pruning.
