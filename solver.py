import random


def decode(square):
    return ((square // 3) + 1, (square % 3) + 1)


def decode_node(node):
    if not isinstance(node, tuple):
        return node
    rv = dict()
    for i in range(len(node)):
        rv[decode(i)] = node[i]
    return rv


def encode(square):
    return (square[0] - 1) * 3 + (square[1] - 1)


def encode_node(node, my_symbol, other_symbol):
    if not isinstance(node, dict):
        return node
    return tuple([from_symbol(value, my_symbol, other_symbol)
                  for key, value in sorted(node.items())])


def first_difference(parent, child):
    for i in range(len(parent)):
        if parent[i] != child[i]:
            return i


def from_symbol(symbol, my_symbol, other_symbol):
    if symbol == my_symbol:
        return 1
    elif symbol == other_symbol:
        return - 1
    else:
        return 0


def get_children(node, current):
    rv = list()
    if winner(node) == 0:
        for i in range(len(node)):
            if node[i] == 0:
                rv.append(node[:i] + tuple([current]) + node[i + 1:])
    return rv


def get_move(node, my_symbol, other_symbol):
    encoded_node = encode_node(node, my_symbol, other_symbol)
    best_child = negamax(encoded_node,
                         - float("inf"),
                         float("inf"),
                         from_symbol(my_symbol, my_symbol, other_symbol))[1]
    return decode(first_difference(encoded_node, best_child))


# https://en.wikipedia.org/wiki/Negamax; 2016-03-03
def negamax(node, alpha, beta, color):
    children = get_children(node, color)
    if len(children) == 0:
        return (color * winner(node), None)
    best_child = None
    for child in children:
        value = - (negamax(child, - beta, - alpha, - color)[0])
        if value > alpha:
            alpha = value
            best_child = child
        if value >= beta:
            return (beta, best_child)
    return (alpha, best_child)


def to_symbol(value, my_symbol, other_symbol):
    if value == 1:
        return my_symbol
    elif value == - 1:
        return other_symbol
    else:
        return 0


def winner(node):
    decoded_node = decode_node(node)
    winning_states = [((1, 1), (1, 2), (1, 3)),
                      ((2, 1), (2, 2), (2, 3)),
                      ((3, 1), (3, 2), (3, 3)),
                      ((1, 1), (2, 1), (3, 1)),
                      ((1, 2), (2, 2), (3, 2)),
                      ((1, 3), (2, 3), (3, 3)),
                      ((1, 1), (2, 2), (3, 3)),
                      ((1, 3), (2, 2), (3, 1))]
    for state in winning_states:
        first = decoded_node[state[0]]
        if (first == decoded_node[state[1]] == decoded_node[state[2]] != 0):
            return first
    return 0
